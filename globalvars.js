/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/

var userid;
var reservation_array = [];
var locations = {};
var completed_reservations = 0;
var max_reservations;
var timeouts = []
var warn_ack = false;
var lastlookahead; 
var reserv_index = 0;
var retry_reservations = true;
var reserv_fails = 0;

function resetGlobalVars() {
    userid = undefined;
    reservation_array = [];
    locations = {};
    completed_reservations = 0;
    max_reservations = undefined;
    timeouts = []
    warn_ack = false;
    lastlookahead = undefined; 
    reserv_index = 0;
    retry_reservations = true;
    reserv_fails = 0;
}