/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/

function initlogin() {
    console.log("initlogin being called")
    $("#login-indicator").show()
    if ($("#docEmail1").val() == "") {
        modalshow("Failed to log in", "Please enter an email address to login with. This field cannot be empty.")
        $("#login-indicator").hide()
        return
    }

    // Empty password will be handled by DOC API
    // Token is just a UUID4 token, I need to see if this is sustainable but this closely emulates what they sent in the real app, i guess
    var token = uuidv4()
    var method = "GET"
    var async = true
    var request = new XMLHttpRequest()

    request.onload = function() {
        try {
            var data = request.responseText;
            var obj = JSON.parse(data)
        } catch {
            $("#login-indicator").hide()
            modalshow("Failed to log in", "The JSON data failed to load from the server due to a parsing error. Please try again later.")
            return
        }

        try {
            userid = obj['customer']['id']
            localStorage.setItem("userid", userid)
            $("#loginsec").hide()
            $("#reservation-step1").show()
            $("#listofreservations").show()
            $("#login-indicator").hide()
            getdininglocs()
        } catch {
            if (obj['status'] == "error") {
                $("#login-indicator").hide()
                modalshow("Failed to log in", "The username & password you entered is incorrect. Please enter the right username/password.")
                return
            } else {
                $("#login-indicator").hide()
                modalshow("Failed to log in", "An unknown error occurred while logging in. Please reload the app, then try again.")
                return
            }
        }
    }

    request.onerror = function() {
        $("#login-indicator").hide()
        modalshow("Failed to log in", "The request to login failed with an unknown error. This could be due to you having no internet connection, or being unable to contact the server. Check your internet connection, reload the app, then try again.")
        return
    }

    request.ontimeout = function() {
        $("#login-indicator").hide()
        modalshow("Failed to log in", "The request to login failed with a timeout after 30 seconds. This is likely due to an unstable internet connection, or the server being overloaded. Check your internet connection, reload the app, then try again.")
        return
    }

    request.open(method, "https://api.dineoncampus.com/v1/customer/authenticate.json?email=" + $("#docEmail1").val() + "&password=" + $("#docPassword1").val() + "&token=" + token, true)
    request.timeout = 30000
    request.send()
    $("#docEmail1").val() = ""
    $("#docPassword1").val() = ""
}

// Code to allow for hitting enter on login screen to login