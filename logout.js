/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/

function confirmlogout() {
    $("#confirmlogoutModal").modal("show")
}

function logout() {
    $("#logout-close-button").hide()
    $("#logout-progress").show()
    // The animation here is so awesome, just wait 250ms for logout
    setTimeout(logout2, 250)
}

function logout2() {
    localStorage.removeItem("userid")
    $("#reservation-step1").hide()
    $("#loginsec").show()
    $("#logout-close-button").show()
    $("#logout-progress").hide()
    resetGlobalVars()
    $("#listofreservations").hide()
    $("#confirmlogoutModal").modal("hide")
}