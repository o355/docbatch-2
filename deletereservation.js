/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/
function delete_reservation(uuid) {
    for (var i = 0; i <= reservation_array.length; i++) {
        if (reservation_array[i].uuid == uuid) {
            reservation_array.splice(i, 1)
            break
        }
    }
    $("#currentreservations").empty()
    append_reservation_v2(reservation_array)
    $("#numreservations").html(reservation_array.length)
    if (reservation_array.length <= 0) {
        $("#skip-to-s3").hide()
        $("#submitbutton-skip3").hide()
        $("#clearall-button").hide()
    }

    if (reservation_array.length <= 19) {
        $("#submitbutton-step3").attr("onclick", "s3_to_s4()")
    }
}

function delete_all_reservations() {
    reservation_array = []
    append_reservation_v2(reservation_array)
    $("#currentreservations").empty()
    $("#numreservations").html(0)
    $("#skip-to-s3").hide()
    $("#submitbutton-skip3").hide()
    $("#clearall-button").hide()
    $("#submitbutton-step3").attr("onclick", "s3_to_s4()")
}