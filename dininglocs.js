/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/
function getdininglocs() {
    // When starting your own copy of DOCBatch 2, change this to your own instance of a DOCBatch API.
    var url = "https://api.docbatch.app/api/v1/locations"
    var request = new XMLHttpRequest()
    var method = "GET"
    var async = true
    request.onload = function() {
        var data = request.responseText;
        try {
            var obj = JSON.parse(data);
        } catch (err) {
            modalshow("Failed to parse dining location data", "Couldn't parse the returned dining location data from DineOnCampus. Reload the app, then try again.")
        }
        var template_html;
        $("#dining-locations").empty()
        Object.keys(obj['data']).forEach(function (k) {
            //template_html = s1_returntemp(obj['data'][k]['name'], "" + k + "")
            //$("#dining-locatons").append(template_html)
            locations[k] = obj['data'][k]["name"]
        })

        $(function() {
            Object.keys(locations).forEach(function (k) {
                $("#dining-locations").append(s1_returntemp(locations[k], k))
            })
            $("#submitbutton-step1").show()
            $("#submitbutton-quick").show()
            $("#loadingwarn").hide()
        })
    }

    request.onerror = function() {
        modalshow("Failed to get dining location data", "The request to get dining location data failed. This is likely due to having no internet connection, or not being able to contact the server. Check your internet connection, reload the app, then try again.")
    }

    request.ontimeout = function() {
        modalshow("Failed to get dining location data", "The request to get dining location data failed due to a request timeout after 30 seconds. This is likely due to having an unstable internet connection. Check your internet connection, reload the app, then try again.")
    }

    request.open(method, url, async)
    request.timeout = 30000
    request.send()
}