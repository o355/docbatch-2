/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/

$(function () {
    if (localStorage.getItem("userid") != null) {
        userid = localStorage.getItem("userid")
        $("#loginsec").hide()
        $("#reservation-step1").show()
        $("#listofreservations").show()
        getdininglocs()
    }

    $("#datetimepicker3").datetimepicker({
        format: 'L'
    })

    $("#datetimepicker4").datetimepicker({
        format: 'LT'
    })

    $("#numberOfReservations1").val(1)

    $("#datetimepicker3").datetimepicker("clear")
    $("#datetimepicker4").datetimepicker("clear")
    resetdors()
    $("#retryReservations").prop("checked", true)
    $("#datepicker-rawinput").val("")
    $("#timepicker-rawinput").val("")
    $("#numberOfReservations1").val("1")
})