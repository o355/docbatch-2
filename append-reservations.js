/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/

// Date should be a readable date from the raw date input field (09/05/2020)
// Time should be a readable date from the raw time input field (1:50 PM)
// Dining Location should be the ID of a dining location.
function append_reservation(date, time, dininglocation, uuid) {
    var diningloc_name = locations[dininglocation]
    $("#currentreservations").append("<li><button type='button' class='btn btn-danger delete-button' onclick=\"delete_reservation('" + uuid + "')\"><i class='fa fa-trash delete-button-icon'></i></button>" + diningloc_name + " on " + date + " at " + time + "</li>")
}

// v2 of append reservation - just takes a reservation array, and figures out the rest.
function append_reservation_v2(reservarray, deleteallowed, freshstart) {
    if (freshstart == true) {
        $("#currentreservations").empty()
    }
    for (var i = 0; i < reservarray.length; i++) {
        var diningloc_name = locations[reservarray[i].location]
        var date = reservarray[i].momentobj.format("dddd, MMM D, YYYY")
        var time = reservarray[i].momentobj.format("h:mm A")
        var uuid = reservarray[i].uuid
        if (deleteallowed == false) {
            $("#currentreservations").append("<li>" + diningloc_name + " on " + date + " at " + time + "</li>")
        } else {
            $("#currentreservations").append("<li><button type='button' class='btn btn-danger delete-button' onclick=\"delete_reservation('" + uuid + "')\"><i class='fa fa-trash delete-button-icon'></i></button>" + diningloc_name + " on " + date + " at " + time + "</li>")
        }
    }
}