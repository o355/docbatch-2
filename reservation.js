/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/

class Reservation {
    // Date should be standard YYYY-mm-dd date (2020-09-05)
    // Hour should be an integer between 0 and 23
    // Minute should be an integer between 0 and 59
    // Location should be a string with an ID of a dining location
    constructor(date, hour, minute, location, momentobj, uuid) {
        this.date = date;
        this.hour = hour;
        this.minute = minute;
        this.location = location;
        this.momentobj = momentobj;
        this.uuid = uuidv4()
    }
}