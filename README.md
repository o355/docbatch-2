# DOCBatch 2
The DOCBatch that started it all, before DOCBatch 3.

# Spaghet-o-meter
All projects made before late 2020 were focused more on development speed rather than reusable code. As I was making DOCBatch 2 while learning how to make reusable code, this is the only project that's more function-focused than anything else.

There's still a lot of spaghetti and weirdness happening here (DOCBatch 3 improves this vastly). 6/10 on my Spaghet-o-meter

# What is DOCBatch 2?
DOCBatch 2 is the WPI Dine On Campus Batch Reserver tool, otherwise known as DOCBatch.

When getting to WPI for the fall semester, you needed dining reservations to get food. The idea was to limit overall capacity (due to COVID), in which you'd reserve ahead of time for when you were going to go eat.

Unfortunately, you can't really time when hunger calls. Dining halls were meant to be walk in, get food, walk out. Not only was the Dine On Campus app (at the time) extremely slow and laggy, you'd need to reserve up to 2 hours ahead of time because everyone was using it.

Cue me, being hackerman a few days into college. I found a flaw in the Dine On Campus API, in which you could still make a reservation for a full dining slot, because they were only doing client-side validation. With that, I made DOCBatch, a web wrapper for the Dine On Campus API. 

# Why make DOCBatch?
To solve a few problems:
* Make it easy to get food really quickly (DynamoDB caching via the API, and reserve for now)
* Make it easy to reserve days in advance (the batch reserver part)
* Solve the issue of the Dine On Campus app sucking

DOCBatch continued to be very useful for points 1 and 3 (with point 2 becoming moot because of this flaw, and people not going to dining halls as much as the year went on).

DOCBatch 1/2 solved these problems pretty well, although there were issues with the UI feeling dated and more confusing as additional features were added (that's where DB3 comes in and saves the day).

# Setup
DOCBatch 2 is exclusively a front-end system that needs a static webserver. It'll run on the current DOCBatch API without any issues - although you'll likely want to get your own copy of the DOCBatch API running for your specific Dine On Campus location.

Read more at https://gitlab.com/o355/docbatch-api.

Once you're done, the only real modification you have to make is in `dininglocs.js`, and point that at your preferred DOCBatch API server. You'll likely want to change `manifest.json` so that the app's manifest points to your servers, and add some iconography (apple-196x196.png, apple-227x227.png, etc etc).

# Privacy
Logging in is done via sending credentials to the Dine On Campus API authentication method, just like how the website & apps do it. DOC has no form of oAuth or another method of logging in, which does create potential privacy concerns.

The only thing DOCBatch stores is your Dine On Campus User ID, which is what identifies you to their systems. 

# License
As a major web application, DOCBatch 2 is licensed under the AGPL v3.0. 

Although DOCBatch spent its entire life closed-source (and I did such to protect DOCBatch and myself for usage during the 2020-21 school year), the intention of DOCBatch was to improve upon Dine On Campus' reservation system. It's in my belief that if you want to improve upon DOCBatch, you should open-source your changes so others can improve upon what you make.

The DOCBatch API will remain under the MIT License, as it's a more down-to-earth API that users will only interact with to grab location data.

DOCBatch 2 makes use of the ics.js, filesaver.js and blob.js libraries, which are all under the MIT License.

I had to remove Polymer (which was used as part of an attempt to make DOCBatch into a PWA), as it's liensed under the Original BSD License, which isn't compatible with the AGPL v3 license. These files don't affect DOCBatch 2's functionality - they were never used.