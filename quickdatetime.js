/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/

function quickdate(duration) {
    var momentobj = moment()
    if (duration == "today") {
        var todaysdate = momentobj.format("MM/DD/YYYY")
        $("#datepicker-rawinput").val(todaysdate)
        return
    } else if (duration == "tomorrow") {
        var tomorrowsdate = momentobj.add(1, "day").format("MM/DD/YYYY")
        $("#datepicker-rawinput").val(tomorrowsdate)
        return
    }
}

function quicktime(duration) {
    var currenttime = moment()
    var lookaheadtime;
    var minutedelta;
    var currentdate_field;
    var lastlookahead = duration;

    if (duration == "0") {
        minutedelta = determine_closest_interval(currenttime.minute())
        if (minutedelta == 0) {
            currenttime.minute(0)
            $("#timepicker-rawinput").val(currenttime.format("h:mm A"))
            $("#datepicker-rawinput").val(currenttime.format("MM/DD/YYYY"))
            return
        } else if (minutedelta == 30) {
            currenttime.minute(30)
            $("#timepicker-rawinput").val(currenttime.format("h:mm A"))
            $("#datepicker-rawinput").val(currenttime.format("MM/DD/YYYY"))
            return
        } else if (minutedelta == 60) {
            currenttime.hour(currenttime.hour() + 1).minute(0)
            $("#timepicker-rawinput").val(currenttime.format("h:mm A"))
            $("#datepicker-rawinput").val(currenttime.format("MM/DD/YYYY"))
            return
        }
    } else {
        lookaheadtime = currenttime

        if (duration == "30") {
            lookaheadtime.add("30", "m")
        } else if (duration == "60") {
            lookaheadtime.add("60", "m")
        } else if (duration == "120") {
            lookaheadtime.add("120", "m")
        } else if (duration == "240") {
            lookaheadtime.add("240", "m")
        }

        minutedelta = determine_closest_interval(lookaheadtime.minute())

        if (minutedelta == 0) {
            lookaheadtime.minute(0)
            $("#timepicker-rawinput").val(lookaheadtime.format("h:mm A"))
            $("#datepicker-rawinput").val(lookaheadtime.format("MM/DD/YYYY"))
            return
        } else if (minutedelta == 30) {
            lookaheadtime.minute(30)
            $("#timepicker-rawinput").val(lookaheadtime.format("h:mm A"))
            $("#datepicker-rawinput").val(lookaheadtime.format("MM/DD/YYYY"))
            return
        } else if (minutedelta == 60) {
            lookaheadtime.hour(lookaheadtime.hour() + 1).minute(0)
            $("#timepicker-rawinput").val(lookaheadtime.format("h:mm A"))
            $("#datepicker-rawinput").val(lookaheadtime.format("MM/DD/YYYY"))
            return
        }
    }
}

// Determine the nearest 30 minute interval depending on the time. 60 minutes = the next hour at :00, while 0 minutes = the current hour at :00.
// This is so you can line up Dine On Campus' 30 minute intervals without getting caught out with wacky 15-minute interval times.
// Lets take a minute at a 5pm interval.
// A value of 0 is returned if the minute is <= 14 (5:00 pm - 5:14 pm), meaning the next 30-minute interval is 5:00 pm.
// A value of 30 is returned if the minute is <= 44 but >= 15 (5:15 pm - 5:44 pm), meaning the next 30 minute interval is 5:30pm.
// A value of 60 is returned if the minute is >= 45 (5:45 pm - 5:59 pm), meaning the next 30 minute interval is 6:00pm.
function determine_closest_interval(minute) {
    if (minute <= 14) {
        return 0
    } else if (minute <= 44) {
        return 30
    } else {
        return 60
    }
}

function incrementtime(duration) {
    if ($("#datepicker-rawinput").val() != "" && $("#timepicker-rawinput").val() != "") {
        var formtime = moment($("#datepicker-rawinput").val() + " " + $("#timepicker-rawinput").val(), "MM/DD/YYYY h:mm A")
        formtime.add(duration, "m")
        $("#datepicker-rawinput").val(formtime.format("MM/DD/YYYY"))
        $("#timepicker-rawinput").val(formtime.format("h:mm A"))
    }
}