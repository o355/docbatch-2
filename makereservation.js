/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/

function submitreservation(finalres, warn_ack, quickreserve) {
    var dininglocation = $("input[name='dininglocation_input']:checked").val()
    var date = $("#datepicker-rawinput").val()
    var time = $("#timepicker-rawinput").val()
    if (date == "" || date == undefined || date == " ") {
        modalshow("Date not entered", "Please enter a date before making a reservation.")
        return
    }

    if (time == "" || time == undefined || time == " ") {
        modalshow("Time not entered", "Please enter a time before making a reservation.")
        return
    }
    var moment_object = moment(date + " " + time, "MM/DD/YYYY h:mm A")
    var api_date = moment(date, "MM/DD/YYYY").format("YYYY-MM-DD")
    var api_hour = moment(date + " " + time, "MM/DD/YYYY h:mm A").hour()
    var api_minute = moment(date + " " + time, "MM/DD/YYYY h:mm A").minute()
    if (api_hour == NaN || api_minute == NaN) {
        modalshow("Not a number error", "Make sure your date & time are formatted correctly before making a reservation.")
        return
    }
    var extrareservations = parseInt($("#numberOfReservations1").val())

    if (extrareservations <= 0) {
        modalshow("Reoccurring Reservations Invalid", "Reoccurring Reservations must be a number that is 1 or greater. Please check the input field.")
        return
    }

    var days_to_reserve = []
    // Fun logic to check what days are checked, and to add them to an array of days checked for additional reservations.
    if ($("#sundayDOR").prop("checked")) {
        days_to_reserve.push(0)
    }
    if ($("#mondayDOR").prop("checked")) {
        days_to_reserve.push(1)
    }
    if ($("#tuesdayDOR").prop("checked")) {
        days_to_reserve.push(2)
    }
    if ($("#wednesdayDOR").prop("checked")) {
        days_to_reserve.push(3)
    }
    if ($("#thursdayDOR").prop("checked")) {
        days_to_reserve.push(4)
    }
    if ($("#fridayDOR").prop("checked")) {
        days_to_reserve.push(5)
    }
    if ($("#saturdayDOR").prop("checked")) {
        days_to_reserve.push(6)
    }

    if (days_to_reserve.length == 0 && extrareservations >= 2) {
        modalshow("No days of week selected for reoccurring reservations", "Please select at least one day of the week for reoccurring reservations.")
        return
    }

    console.log(reservation_array.length + extrareservations)
    //if (reservation_array.length + extrareservations >= 20 && warn_ack != true && finalres) {
    //    modalwarn(finalres)
    //    $(".reoccur-span").html(reservation_array.length + extrareservations)
    //    return
    //}

    if (reservation_array.length + extrareservations >= 20) {
        $("#submitbutton-step3").attr("onclick", "s3_to_s4_confirm()")
    }
    var newreservationlength = reservation_array.length + extrareservations
    $(".reoccur-span").html(newreservationlength)

    console.log(moment_object)
    var reserv_array_oldlen = reservation_array.length
    var new_reservation = new Reservation(api_date, api_hour, api_minute, dininglocation, moment_object)
    reservation_array.push(new_reservation)
    append_reservation(moment_object.format("dddd, MMM D, YYYY"), time, dininglocation, new_reservation.uuid)
    
    if (extrareservations > 1) {
        var extended_date = moment(date, "MM/DD/YYYY")
        var extended_date_tostr = extended_date.format("MM/DD/YYYY")
        var extended_moment = moment(extended_date_tostr + " " + time, "MM/DD/YYYY h:mm A")
        // i++ gets handled inside of the loop because sometimes dow doesn't line up
        for (var i = 2; i <= extrareservations; ) {
            extended_date = extended_date.add(1, 'days')
            extended_date_dow = extended_date.day()
            extended_date_tostr = extended_date.format("MM/DD/YYYY")
            extended_moment = moment(extended_date_tostr + " " + time, "MM/DD/YYYY h:mm A")
            if (days_to_reserve.includes(extended_date_dow)) {
                var new_reservation = new Reservation(extended_date.format("YYYY-MM-DD"), api_hour, api_minute, dininglocation, extended_moment)
                reservation_array.push(new_reservation)
                append_reservation(extended_date.format("dddd, MMM D, YYYY"), time, dininglocation, new_reservation.uuid)
                i++
            } else {
                continue
            }
        }
    }
    if (reservation_array.length >= 1) {
        $("#skip-to-s3").show()
        $("#clearall-button").show()
    }
    
    $("#numreservations").html(reservation_array.length)
    if (quickreserve) {
        $("#reservation-step1").hide()
        $("#reservation-step3").show()
        $("#timepicker-rawinput").val("")
        $("#datepicker-rawinput").val("")
        if (reservation_array.length <= 39) {
            $("#reservationCollapse").collapse("show")
        } else {
            $("#reservationCollapse").collapse("hide")
        }
        $("#")
        return
    }

    if (finalres) {
        $("#reservation-step2").hide()
        $("#reservation-step3").show()
        if (reservation_array.length <= 39) {
            $("#reservationCollapse").collapse("show")
        } else {
            $("#reservationCollapse").collapse("hide")
        }
        $("#numberOfReservations1").val(1)
    } else {
        if (reserv_array_oldlen <= 9 && reservation_array.length >= 10) {
            $("#reservationCollapse").collapse("hide")
        }

        if (reserv_array_oldlen == 0 && reservation_array.length > 0 && reservation_array.length <= 9) {
            $("#reservationCollapse").collapse("show")
        }
        $("#reservation-step2").hide()
        $("#reservation-step1").show()
        $("#numberOfReservations1").val(1)
    }
}