/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/

function submitreservation_xhr(reservobj, reschedule) {
    var url = "https://api.dineoncampus.com/v1/reservations"
    var request = new XMLHttpRequest()
    // Probably not needed
    if (reschedule != true) {
        $("#failreserv-span").hide()
        $("#reschedule-success-span").hide()
    }
    $("#fail-fullslot").hide()
    var resrv_obj = reservobj
    var method = "POST"
    var async = true
    var json_data = {
        "customer_id": userid,
        "date": reservobj.date,
        "friends": 0,
        "hour": reservobj.hour,
        "location_id": reservobj.location,
        "minute": reservobj.minute
    }

    $("#reserve-details-location").html(locations[reservobj.location])
    $("#reserve-details-date").html(reservobj.momentobj.format("dddd, MMM D, YYYY"))
    $("#reserve-details-time").html(reservobj.momentobj.format("h:mm A"))

    request.open(method, url, true)
    request.timeout = 30000
    // For testing on DOCAPI, comment out the Content Type Header.
    request.setRequestHeader("Content-Type", "application/json")
    request.send(JSON.stringify(json_data))
    // Dry run enabled. not actually doing anything.


    request.onload = function() {
        var failed_noretry = false
        var data = request.responseText
        var obj = JSON.parse(data)
        console.log(obj)
        console.log(obj['status'])
        console.log(obj['msg'])
        if (obj['status'] != "success") {
            if (obj['status'] == "error" && obj['msg'] == "The time slot you requested is no longer available.") {
                if (retry_reservations) {
                    reschedule_reservation(resrv_obj)
                    return
                } else {
                    failed_noretry = true
                    reserv_fails = reserv_fails + 1
                }
            } else {
                $("#reservation-step4").hide()
                $("#listofreservations").hide()
                $("#reservation-failed-knownerror").show()
                return
            }
        }
        var nexttimeout = 0;
        if (reschedule) {
            $("#failreserv-span").hide()
            $("#reschedule-success-span").show()
            nexttimeout = 500
        } else if (failed_noretry) {
            $("#fail-fullslot").show()
            nexttimeout = 1000
        }
        completed_reservations = completed_reservations + 1
        $("#completed").html(completed_reservations)
        if (completed_reservations == max_reservations) {
            $("#reserve-details").hide()
            $("#mainreserv-span").hide()
            setTimeout(function () {
                $("#reschedule-success-span").hide()
            }, 2000)
            if (reserv_fails >= 1) {
                $("#failed-requests").html(reserv_fails)
                $("#completed-span-fails").show()
                setTimeout(function () {
                    $("#reservation-step4").hide()
                    $("#reservation-finished-slotsfull").show()
                    $("#listofreservations").hide()
                }, 4000)
            } else {
                $("#completed-span-nofails").show()
                setTimeout(function () {
                    $("#reservation-step4").hide()
                    $("#reservation-finished").show()
                    $("#listofreservations").hide()
                }, 4000)
            }
        } else {
            setTimeout(executereservation.bind(null, completed_reservations), nexttimeout)
        }
    }

    request.onerror = function() {
        //for (var i = 0; i < timeouts.length; i++) {
        //    clearTimeout(timeouts[i]);
        //}
        $("#reservation-step4").hide()
        $("#listofreservations").hide()
        $("#reservation-failed-error").show()
    }

    request.ontimeout = function() {
        //for (var i = 0; i < timeouts.length; i++) {
        //    clearTimeout(timeouts[i]);
        //}
        $("#reservation-step4").hide()
        $("#listofreservations").hide()
        $("#reservation-failed-timeout").show()
    }
}

function reschedule_reservation(reservobj) {
    $("#failreserv-span").show()
    var newreservobj = reservobj
    newreservobj.momentobj = newreservobj.momentobj.add("30", "m")
    newreservobj.date = newreservobj.momentobj.format("YYYY-MM-DD")
    newreservobj.hour = newreservobj.momentobj.get("hour")
    newreservobj.minute = newreservobj.momentobj.get("minute")
    $(".fail-retry-slot").html(newreservobj.momentobj.format("h:mm A [on] MM/DD/YYYY"))
    setTimeout(submitreservation_xhr.bind(null, newreservobj, true), 1000)
}

function preparereservation_xhr() {
    var timeout_time = 0;
    if ($("#retryReservations").prop("checked")) {
        retry_reservations = true
    } else {
        retry_reservations = false
    }
    append_reservation_v2(reservation_array, false, true)
    $("#clearall-button").hide()
    $("#completed").html("0")
    $(".total-reservations").html(reservation_array.length)
    $("#reservationCollapse").collapse("hide")
    max_reservations = reservation_array.length
    executereservation(0)
    //for (var i = 0; i < reservation_array.length; i++) {
    //    timeouts.push(setTimeout(submitreservation_xhr.bind(null, reservation_array[i]), timeout_time))
    //    timeout_time = timeout_time + 3000
    //}
}

function executereservation(index) {
    submitreservation_xhr(reservation_array[index])
}