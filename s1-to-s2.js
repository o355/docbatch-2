/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/

function s1_to_s2 () {
    var dininglocation = $("input[name='dininglocation_input']:checked").val()
    console.log(dininglocation)
    if (dininglocation == undefined || dininglocation == "") {
        modalshow("Dining location not selected", "Please select a dining location to continue.")
        return
    }
    $("#selected-dininglocation").html(locations[dininglocation])
    $("#reservation-step1").hide()
    $("#reservation-step2").show()
}

function s2_to_s1() {
    $("#reservation-step2").hide()
    $("#reservation-step1").show()
}

function s3_to_s4() {
    if (reservation_array.length == 0) {
        modalshow("You must have 1 reservation to continue", "Please make at least 1 reservation to continue to Step 4.")
        return
    }
    preparereservation_xhr()
    $("#reservation-step3").hide()
    $("#reservation-step4").show()
}

function s3_to_s4_confirm() {
    modalwarn()
}

function s3_to_s1() {
    $("#reservation-step3").hide()
    $("#reservation-step1").show()
    if (reservation_array.length >= 10) {
        $("#reservationCollapse").collapse("hide")
    }
}

function s1_to_s3() {
    $("#reservation-step1").hide()
    $("#reservation-step3").show()
    if (reservation_array.length <= 39) {
        $("#reservationCollapse").collapse("show")
    } else {
        $("#reservationCollapse").collapse("hide")
    }
}

function viewchangelog() {
    $("#changelogModal").modal("show")
}

