/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/

function generate_ics() {
    var cal = ics();
    for (var i = 0; i < reservation_array.length; i++) {
        var location = locations[reservation_array[i].location]
        var reservation_time_human = (reservation_array[i].momentobj).format("MM/DD/YYYY [at] h:mm A")
        var reservation_time_start = (reservation_array[i].momentobj).format("MM/DD/YYYY h:mm:ss A")
        var reservation_time_end = ((reservation_array[i].momentobj).add("30", "minutes")).format("MM/DD/YYYY h:mm:ss A")
        cal.addEvent(location + " - Reservation", 
        "You have a dining reservation at " + location + " on " + reservation_time_human + ".", 
        location, 
        reservation_time_start, 
        reservation_time_end)
    }
    cal.download()
}