/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/

importScripts('https://storage.googleapis.com/workbox-cdn/releases/5.0.0/workbox-sw.js');

const CACHE = "wpidocbatch-page";

const offlineFallbackPage = "offline.html";

self.addEventListener("message", (event) => {
  if (event.data && event.data.type === "SKIP_WAITING") {
    self.skipWaiting();
  }
});

self.addEventListener('install', async (event) => {
  event.waitUntil(
    caches.open(CACHE)
      .then((cache) => cache.add(offlineFallbackPage))
  );
});

if (workbox.navigationPreload.isSupported()) {
  workbox.navigationPreload.enable();
}

// Comment out network handler to try and improve JS caching, I guess?
//self.addEventListener('fetch', (event) => {
//  if (event.request.mode === 'navigate') {
//    event.respondWith((async () => {
//      try {
//        const preloadResp = await event.preloadResponse;
//
//        if (preloadResp) {
//          return preloadResp;
//       }
//
//        const networkResp = await fetch(event.request);
//        return networkResp;
//      } catch (error) {
//
//        const cache = await caches.open(CACHE);
//        const cachedResp = await cache.match(offlineFallbackPage);
//        return cachedResp;
//      }
//    })());
//  }
//});
