/*  DOCBatch 2
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.  
*/

function s1_returntemp(diningloc, diningid) {
    var rawhtml = "<div class='form-check'><input class='form-check-input' name='dininglocation_input' type='radio' value='" + diningid + "'><label class='form-check-label'>" + diningloc + "</label></div>"
    return rawhtml
}